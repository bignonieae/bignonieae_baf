# Bignonieae da Mata Atlântica

## Folder structure

```
.
├── 01_scripts
├── 02_data
├── 03_figs
├── 04_output
├── 05_enm_outputs
├── 06_docs
├── README.html
├── README.md
├── SD
└── Bignonieae_BAF.Rproj
```

## Variables ambientales

  - [x] origen del dataset: CHELSA
  - [x] resolución: 5min
  - [x] extent: ya
  - [ ] proyecciones pasado: VERIFICAR!
  - [ ] PCA? proyección de PCA?

## Ocurrencias

  - [x] actualizar a v14
  - [ ] decidir sobre "marginally" - ver correo LML -> refazer seleção 
  
## Biomas análise

 - [ ] chequear que esté bien y faça sentido-
 - [ ] decidir si bioma o wet dry 
 - [ ] analizarle las condiciones ambientales a los biomas - localizaciones de las especies
 
## Posición filogenética

 - [ ] Mantener género pero pensar si vale la pena hacer esto
 
## Niche analysis

 - [ ] ¿Es necesario?
 - [ ] ¿dynRB?
 
## Alphahull

 - [ ] Estaba haciendo eso para tener estimativas de riqueza independientes de modelamiento, not EOO, pero tampoco AOO. ¿Sigue valiendo la pena?
 - [ ] Faltaba ver las especies disyuntas pero se pueden sumar y después ver
 
## ENM

 - [ ] ¿Es necesario?
 - [ ] Parametrizar
 - [ ] ¿Proyectar?
 
 
 
 


   
 