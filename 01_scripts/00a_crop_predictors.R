#lee los rasters del extent que va a ser
library(raster)
library(dplyr)

dataset <- "chelsa30s"
ff <- list.files(paste0("./02_data/env/", dataset, "/bio"), full.names = T, pattern = ".tif$")
ss <- stack(ff)
ss <- subset(ss, setdiff(1:19, c(8,9,18,19)))

# PCA
    sr <- dismo::randomPoints(ss, 10000)
    vvv <- extract(ss, sr)
    vvv <- vvv[complete.cases(vvv),]
    write.csv(vvv, "./04_output/env_sample.csv")
    #sr <- read.csv("./04_output/chelsa_sample.csv", row.names = 1)
    sr.st <- scale(vvv)
    pca.v <- prcomp(sr.st)

summary.pca <- summary(pca.v)
axis.nb <- which(summary.pca$importance["Cumulative Proportion",] >= 0.95)[1]
eigenvariables <- predict(ss, pca.v, index = 1:axis.nb)
save(eigenvariables, file = paste0("./02_data/env/", dataset, "/eigen/eigen.rda"))
writeRaster(eigenvariables,
            filename = paste0("./02_data/env/",dataset,"/eigen/eigen_"),
                              bylayer = "T",
                              suffix = "numbers",
                              overwrite = T,
                              format = "GTiff")
