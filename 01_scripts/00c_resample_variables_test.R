#resamplea variables para que sirvan localmente
#update: pero en vez de resamplear las eigen vamos a resamplear los raw y crear las eigen de nuevo.
library(raster)
library(dplyr)

files <- list.files("./02_data/env/chelsa30s/bio", full.names = T)
chelsa <- stack(files[4:22])

f5m <- list.files("./02_data/env/chelsa5m/eigen", full.names = T)
r5m <- raster(f5m[1])
#f <- list.files("./data/env/chelsa10m/eigen/", full.names = T)
#r <- raster(f[1])

#data10m <- getData('worldclim',  var = 'bio', res = 10, path = "./data/env/bio/")
#data5m <- getData("worldclim", var = 'tmax', res = 5, path = "./data/env/bio/")

ex_crop <- crop(r5m, chelsa)
dir.create("./02_data/env/chelsa5m/bio")
for (i in 1:dim(chelsa)[3]) {
chelsa_r <- resample(chelsa[[i]], ex_crop)
writeRaster(chelsa_r, filename = paste0("./02_data/env/chelsa5m/bio/bio",i), bylayer = T, format = "GTiff")
}