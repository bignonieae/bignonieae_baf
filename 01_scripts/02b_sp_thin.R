
# spthin!
dir.create("./04_output/spthin")
localidades.clean %>%
    split(.$NAME1) %>%
    purrr::map(~spThin::thin(loc.data = .x,
                             lat.col = "YCOOR",
                             long.col = "XCOOR",
                             spec.col = "NAME1",
                             thin.par = 20,
                             reps = 99,
                             locs.thinned.list.return = T,
                             write.files = T,
                             out.dir = "./04_output/spthin")
)
length(list.files("./04_output/spthin/"))
