library(dplyr)
#entendiendo los resultados
run <- read.csv("./05_output_enm/runresults_eigen1.csv", row.names = 1)
eval_all <- read.csv("./05_output_enm/eval_all_eigen5m2.csv", row.names = 1) %>%
    rename(species_name = X)
eval_all %>% count(species_name) %>% filter(n != 35) %>% arrange(n)
eval_all %>% count(algoritmo)

# brt
eval_all %>% count(algoritmo, species_name) %>%
    filter(algoritmo == "brt") %>% count(n==5)
brt_yes_no <- table(eval_all$algoritmo, eval_all$species_name) %>% .["brt",] %>%
    data.frame() %>% rename(brt = ".") %>%
    tibble::rownames_to_column(., var = "species_name")
#left_join(brt_yes_no1, brt_yes_no2, by = "species_name") %>% count(brt.x, brt.y)
brt_results <- left_join(brt_yes_no, run)
#brt_results1 <- left_join(brt_yes_no1, run)
#brt_results2 <- left_join(brt_yes_no2, run)
#brt_comparison <- left_join(brt_yes_no1, brt_yes_no2, by = "species_name")
head(brt_results)
head(eval_all)
all_brt <- left_join(eval_all, brt_results, by = "species_name")
#all_brt <- left_join(eval_all, brt_results1, by = "species_name")
names(all_brt)



library(ggplot2)
all_brt %>%
    #filter(omission < 0.1) %>%
    ggplot(aes(x = final.n, y = brt)) +
    #geom_boxplot() +
    #geom_jitter(width = 0.1, alpha = 0.2) +
    #xlim(0,30) +
    geom_point() +
    #geom_hline(yintercept = 0.5, col = "red") +
    geom_vline(xintercept = 30, col = "red") +
    #facet_wrap(~algoritmo) +
    NULL
ggsave("brt n = 10.png")
head(brt_results)
brt_results %>% count(final.n, buffer_type,brt) %>% View()



# erro novo com eigen
head(eval_all)
head(run)
eval_all %>% group_by(algoritmo, species_name) %>% summarize(n = n()) %>%
    tidyr::spread(.,1,"n") %>% View()

