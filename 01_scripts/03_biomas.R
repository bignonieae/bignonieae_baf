#!/usr/bin/env Rscript
library(raster)
library(dplyr)
library(readr)

#especies
cat("reads spp")
presentes.clean <- read_csv("./04_output/Table2_present_clean.csv")

# occs
cat("reads occs")
localidades.clean <- read_csv("./04_output/localidades_presentes_unicas.csv")

cat("reads biome raster")
biomas.r <- raster("./02_data/rasters/biomas_raster_5m.tif")
#biomas.r <- raster("./02_data/biomas_raster_30s.tif")
#1 = amazonia
#2 = caatinga
#3 = cerrado
#4 = baf
#5 = pantanal


#zonaltest
sp_coord <- localidades.clean %>%
    split(.$NAME1) %>%
    purrr::map(~dplyr::select(.x, XCOOR, YCOOR))
sp_rasters <- sp_coord %>%
    purrr::map(~raster::rasterize(.x, biomas.r, field = 1))
sp_zonal <- sp_rasters %>%
    purrr::map(~zonal(x = .x, z = biomas.r, fun = "sum", na.rm = T))
spzonal2 <- sp_zonal %>%
    purrr::map(~t(.x)) %>%
    purrr::map(~tibble::as.tibble(.x)) %>%
    purrr::map(~dplyr::rename(.x,
                              Amazonia = V1,
                              Caatinga = V2,
                              Cerrado = V3,
                              MataAtlantica = V4,
                              Pampa = V5,
                              Pantanal = V6)) %>%
    purrr::map(~dplyr::slice(.x, -1))
spzonal3 <- spzonal2 %>%
    purrr::map2(.x = ., .y = names(spzonal2),
                ~mutate(.x, NAME1 = .y))
spzonal3$`Adenocalymma ackermannii`
biom2 <- bind_rows(spzonal3)
biom2 <- left_join(biom2, presentes.clean)
write_csv(biom2, "./04_output/biomas/presencia_biomas.csv")