#lee los rasters del extent que va a ser
library(raster)
library(dplyr)
#files son los chelsas originales a 30s
files <- list.files("./02_data/env/chelsa30s/bio", full.names = T, pattern = ".tif$")
env_r <- raster(files[1])#one

# lee el shape de BIOMAS!!! estaba dando mal substetando el shape
#biomas
biomas <- rgdal::readOGR("./02_data/shapes/biomas","BR_BIOMAS_IBGE")
#corta env a extent de biomas
env_biomas <- crop(env_r, extent(biomas))
#los rasteriza a todos
biomas.r <- rasterize(biomas, env_biomas, field = biomas@data$CD_LEGENDA)
plot(biomas.r)
biomas.r
unique(values(biomas.r))
#duplica para crear baf
Baf.raster <- biomas.r
#subtitui valores para seleccionar apenas baf
Baf.raster[Baf.raster != 4] <- NA
plot(Baf.raster, add = T)

#salva para que ahora sea solo leer
# writeRaster(biomas.r, "./data/rasters/biomas_raster_10m.tif", overwrite = T)
# writeRaster(Baf.raster, "./data/rasters/ma_raster_10m.tif", overwrite = T)

