library(alphahull)
library(raster)
library(dplyr)
library(purrr)
library(magrittr)
localidades.clean <- read.csv("./04_output/localidades_presentes_unicas.csv", row.names = 1)

nombres <- localidades.clean %>%
    dplyr::select(NAME1, XCOOR, YCOOR) %>%
    count(NAME1) %>%
    filter(n > 2) %>%
    dplyr::select(NAME1) %>%
    droplevels

lista_coord <- localidades.clean %>%
    dplyr::filter(NAME1 %in% nombres$NAME1) %>%
    mutate(NAME1 = factor(NAME1)) %>%
    dplyr::select(NAME1, XCOOR, YCOOR) %>%
    split(.$NAME1)
#un solo alpha no se puede toca variar el alpha para cada especie

biomas.r <- raster("./02_data/rasters/biomas_raster_5m.tif")
raster_final <- raster("./02_data/env/chelsa5m/bio/bio1.tif")

#datos del paper de ellos
library("docxtractr")
#modified by hand: "-" replaced by NA and * to mark disjunct
alphadata <- read_docx("./02_data/alphahull/tped_a_1425505_sm3265.docx")
docx_tbl_count(alphadata)
alphadata <- docx_extract_tbl(alphadata, tbl_number = 1, header = TRUE, trim = TRUE)
alphadata
names(alphadata)#hay un error de formato
names(alphadata)[4] <- "Convex hull range area"
names(alphadata)[5] <- "Alpha hull range area"
names(alphadata)[6] <- "Description year"
head(alphadata)
alphadata <- alphadata[-1,]
write.csv(alphadata,"./02_data/alphahull/alphadata.csv")

class(alphadata$`Number of occurrence records`) <- "numeric"
class(alphadata$`Alpha value`) <- "numeric"
class(alphadata$`Convex hull range area`) <- "numeric"
class(alphadata$`Alpha hull range area`) <- "numeric"
class(alphadata$`Description year`) <- "numeric"

alphadata <- readr::read_csv("./02_data/alphahull/alphadata.csv")
head(alphadata)

alphadata %<>%
    dplyr::select(Species, Number.of.occurrence.records, Alpha.value) %>%
    rename(NAME1 = Species)
head(alphadata)#cada uno con su alfa original

presentes_clean <- read.csv("./04_output/Table2_present_clean.csv", row.names = 1)
endemismo_clean <- read.csv("./04_output/Table4_resumen_endemismo.csv", row.names = 1) %>% filter(endemismo.mod != "absent")
presentes_alpha <- presentes_clean %>%
    left_join(endemismo_clean) %>%
    left_join(alphadata) %>%
    dplyr::select(NAME1, n.clean, Number.of.occurrence.records, endemismo.mod, Alpha.value)
head(presentes_alpha)

###alphas nuevos
#voy a hacer una gran lista que tenga las coordenadas pero tambien los alpha values
dim(localidades.clean)
loc_alpha <- left_join(localidades.clean, presentes_alpha)
dim(loc_alpha)
lista_coord_alpha <- loc_alpha %>%
    dplyr::filter(NAME1 %in% nombres$NAME1) %>%
    dplyr::filter(n.clean > 2) %>%
    mutate(n_dif = n.clean - Number.of.occurrence.records) %>%
    mutate(NAME1 = factor(NAME1)) %>%
    dplyr::select(NAME1, XCOOR, YCOOR, Alpha.value) %>%
    #mutate(Alpha.value = ifelse(is.na(Alpha.value), 1, Alpha.value)) %>%
    #mutate(Alpha.value = 1) %>%
    split(.$NAME1)
length(lista_coord_alpha)
lista_coord_alpha[[1]]
####actualizé a mano los alphas en una movida hermosísima que está en alphahull_update.R
alfas <- read.csv("./04_output/alphahull/alphahull_corregidos.csv") %>% rename(NAME1 = X)
presentes_alphas_corregidos <- left_join(presentes_alpha, alfas)
#repasando el código veo que los alphas que corrijo no siempre vuelven a valores anteriores. si era 8 y pongo 7 y después 8, no vuelve igual... ni ideaaaa entonces no rodé esto de mais fue solo para corregir los errores muy evidentes
#ya supe: era con alpha as.numeric(alpha)
##vuelvo a rodar el codigo que hace los plots para chequear que todo esté bien
dim(localidades.clean)
loc_alpha <- left_join(localidades.clean, presentes_alphas_corregidos)
head(loc_alpha)
count(loc_alpha, alfas)#12 especies con nas
lista_coord_alpha <- loc_alpha %>%
    dplyr::filter(NAME1 %in% nombres$NAME1) %>%
    dplyr::filter(!is.na(alfas)) %>%
    mutate(n_dif = n.clean - Number.of.occurrence.records) %>%
    #filter(n_dif == 0) %>%#los que están bien
    mutate(NAME1 = factor(NAME1)) %>%
    dplyr::select(NAME1, XCOOR, YCOOR, alfas) %>%
    split(.$NAME1)
length(lista_coord_alpha)
alphahulls_alpha <- lista_coord_alpha %>%
    purrr::map( ~ashape(.$XCOOR, .$YCOOR, alpha = unique(.$alfas)))
#listo!
length(alphahulls_alpha)
#129especies
#139dice en 6 de abril e 2019

#toca chequear que los alphahulls sean de fato adequados
names(alphahulls_alpha)
#acá hice un mapa y el poligono solo con y sin poly
#dir.create("./04_output/alphahull_final/")
for (i in 1:length(alphahulls_alpha)) {
    png(filename = paste0("./04_output/alphahull/1 raw alphas/poly_",names(alphahulls_alpha)[i],".png"))
    plot(alphahulls_alpha[[i]], add = F, col = scales::alpha("red",1))
    dev.off()
}
#todo del putas
#
#ahora necesito transformar a shp
source("./01_scripts/alphahull to polygon.R")

library(sp)
par(mfrow = c(5, 5), mar = c(1,1,1,1))
library(igraph)
polygon_list <- alphahulls_alpha %>% purrr::map(~alpha_to_shp(.))

#se quedó haciendo esto# dio mal.
for (i in 1:length(polygon_list)) {
    r <- rasterize(polygon_list[[i]], raster_final, field = 1,
                   filename = paste0("./04_output/alphahull_final/raster/",
                                     names(polygon_list)[i]), overwrite = T, format = "GTiff")
    r2 <- crop(r, polygon_list[[i]],
               filename = paste0("./04_output/alphahull_final/raster/cropped_", names(polygon_list)[i]), overwrite = T, format = "GTiff")
    png(paste0("./04_output/alphahull_final/raster/cropped2_", names(polygon_list)[i],".png"))
    plot(polygon_list[[i]])
    plot(r2, add = T)
    dev.off()
    }


##reorganizé las pstas a mano
#riqueza
#ö faltan las especies que no tienen
sp_list   <- unique(localidades.clean$NAME1)
sp_raster <- names(polygon_list)
(sp_no_alphahull <- setdiff(sp_list, sp_raster))

#aqui voy
loc_no_alphahull <- localidades.clean %>%
    filter(NAME1 %in% sp_no_alphahull) %>%
    dplyr::select(NAME1, XCOOR, YCOOR) %>%
    mutate(NAME1 = droplevels(as.factor(NAME1)))

loc_list <- loc_no_alphahull %>% split(.$NAME1)
for (i in 1:length(loc_list)) {
    r <- rasterize(loc_list[[i]][,c(2,3)], raster_final, field = 1,
                   filename = paste0("./04_output/alphahull/3 no_alphahull/",
                                     names(loc_list)[i]), overwrite = T, format = "GTiff")
    #r2 <- crop(r, r,
    #           filename = paste0("./04_output/alphahull/3 no_alphahull/cropped_",
     #                            names(loc_list)[i]), overwrite = T, format = "GTiff")
    #png(paste0("./04_output/alphahull/3 no_alphahull/cropped_", names(loc_list)[i],".png"))
    #plot(r, add = T)
    #points(loc_list[[i]]$XCOOR, loc_list[[i]]$YCOOR, col = "red", pch = 21)
    #dev.off()
}


####juntar rasters de alpha
all_rasters <- list.files("./04_output/alphahull_final/raster/",
                          pattern = ".+tif$",
                          full.names = T)
cropped_raster <- list.files("./04_output/alphahull_final/raster/",
                             pattern = "cropped",
                             full.names = T)

uncropped_raster <- setdiff(all_rasters, cropped_raster)
ur <- stack(uncropped_raster)
dim(ur)
alpha_rich <- raster::calc(ur, function(x) sum(x, na.rm = T))
#alpha_rich <- sum(ur, na.rm = T)
plot(alpha_rich)
maps::map(,,add = T)
#chequear lso NA
#AHORA lista de esto y suma de alphahulls
writeRaster(alpha_rich, "./04_output/alphahull/Richness_alpha", format = "GTiff", overwrite = T)

v <- values(alpha_rich)
unique(v)
test_mask <- mask(alpha_rich, raster_final)
plot(alpha_rich)
plot(test_mask)
writeRaster(test_mask, "./04_output/alphahull/Richness_alpha_masked", format = "GTiff")
png("./04_output/alphahull/alpha_richness_masked_5m.png")
plot(test_mask)
maps::map(,,,add = T)
dev.off()

biomas <- rgdal::readOGR("./02_data/shapes/biomas","BR_BIOMAS_IBGE")
png("./04_output/alphahull/alpha_richness_masked_biomas_5m.png")
plot(biomas.r, legend = F, col = scales::alpha("white",0))
plot(test_mask, add = T)
plot(biomas, add = T)
maps::map(,,,add = T)
dev.off()
ur_names <- names(ur) %>%
    stringr::str_replace("_", replacement = " ")
ur_spp <- data.frame(NAME1 = ur_names) %>%
    left_join(presentes_alphas_corregidos)
dim(ur_spp)
end <- which(ur_spp$endemismo.mod == "endemic")
ur_endemic <- raster::subset(ur, which(ur_spp$endemismo.mod == "endemic"))
ur_marginally <- raster::subset(ur, which(ur_spp$endemismo.mod == "marginally present"))
ur_present <- raster::subset(ur, which(ur_spp$endemismo.mod == "present"))

alpha_rich_endemic <- sum(ur_endemic, na.rm = T)
alpha_rich_marginally <- sum(ur_marginally, na.rm = T)
alpha_rich_present <- sum(ur_present, na.rm = T)

end_mask <-  mask(alpha_rich_endemic, raster_final)
pres_mask <- mask(alpha_rich_present, raster_final)
marg_mask <- mask(alpha_rich_marginally, raster_final)


png("./04_output/alphahull/alpha_richness_per_group.png", height = 600, width = 600)
par(mfrow = c(2, 2), mar = c(1, 2, 2,1))
plot(biomas.r, col = scales::alpha("white", 0),legend = F, main = "richness")
plot(test_mask, add = T)
plot(biomas, add = T)
maps::map(,,add = T)


plot(biomas.r, col = scales::alpha("white", 0),legend = F, main = "present")
plot(pres_mask, add = T)
plot(biomas, add = T)
maps::map(,,add = T)

plot(biomas.r, col = scales::alpha("white", 0),legend = F, main = "marginally present")
plot(marg_mask, add = T)
plot(biomas, add = T)
maps::map(,,add = T)

plot(biomas.r, col = scales::alpha("white", 0),legend = F, main = "endemic")
plot(end_mask, add = T)
plot(biomas, add = T)
maps::map(,,add = T)
dev.off()

writeRaster(end_mask, "./04_output/alphahull/Richness_alpha_masked_END", format = "GTiff")
writeRaster(pres_mask, "./04_output/alphahull/Richness_alpha_masked_PRES", format = "GTiff")
writeRaster(marg_mask, "./04_output/alphahull/Richness_alpha_masked_MARG", format = "GTiff")

###por nudo del arbol
resumen_endemismo <- read.csv("./04_output/Table4_resumen_endemismo.csv", row.names = 1)
alpha_genera <- left_join(presentes_alphas_corregidos, resumen_endemismo)

ur_Ade <- raster::subset(ur, which(alpha_genera$GENUS == "Adenocalymma"))
ur_Ade_end <- raster::subset(ur, which(alpha_genera$GENUS == "Adenocalymma" &
                                           alpha_genera$endemismo.mod == "endemic"))
alpha_rich_Ade <- sum(ur_Ade, na.rm = T)
alpha_rich_Ade_end <- sum(ur_Ade_end, na.rm = T)
ur_Fri <- raster::subset(ur, which(alpha_genera$GENUS == "Fridericia"))
ur_Fri_end <- raster::subset(ur, which(alpha_genera$GENUS == "Fridericia" &
                                           alpha_genera$endemismo.mod == "endemic"))
alpha_rich_Fri <- sum(ur_Fri, na.rm = T)
alpha_rich_Fri_end <- sum(ur_Fri_end, na.rm = T)

install.packages("sf")

plot(alpha_rich_Ade)
map(,,add=T)
plot(alpha_rich_Ade_end)
map(,,add=T)
plot(alpha_rich_Fri)
map(,,add=T)
plot(alpha_rich_Fri_end)
map(,,add=T)

png("./04_output/alphahull/alpha_richness_Ade_Fri.png", height = 600, width = 600)
par(mfrow = c(2, 2), mar = c(1, 2, 2,1))
plot(biomas.r, col = scales::alpha("white", 0),legend = F, main = "Adenocalymma")
plot(alpha_rich_Ade, add = T)
plot(biomas, add = T)
maps::map(,,add = T)

plot(biomas.r, col = scales::alpha("white", 0),legend = F, main = "Fridericia")
plot(alpha_rich_Fri, add = T)
plot(biomas, add = T)
maps::map(,,add = T)

plot(biomas.r, col = scales::alpha("white", 0),legend = F, main = "Endemic Adenocalymma")
plot(alpha_rich_Ade_end, add = T)
plot(biomas, add = T)
maps::map(,,add = T)

plot(biomas.r, col = scales::alpha("white", 0),legend = F, main = "Endemic Fridericia")
plot(alpha_rich_Fri_end, add = T)
plot(biomas, add = T)
maps::map(,,add = T)
dev.off()

#revisar esas adenocalymma, no?
#hacer una por genero?